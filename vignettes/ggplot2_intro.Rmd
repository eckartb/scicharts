---
title: "ggplot2 - An Introduction"
date: "5/17/2018"
output:
  ioslides_presentation:
    smaller: true
    fig_width: 4
    fig_height: 3
---

```{r setup, include=FALSE}
library(knitr)
knitr::opts_chunk$set(echo = TRUE)
```


## How R Represents Data

* 4 foundational types in R are all vectors:
  - Vector of floating point numbers
  - Vector of integer numbers
  - Vector of character strings
  - Vector of logical values
* A number like 3.5 is really a floating point vector of length one where the first element has the value of 3.5
* Examples:
 - `length(3.5)` # result: 1
 - `typeof(3.5)` # result: "double"
 - `v <- c(2.1,5.3)`  # function 'c' creates vector of length 2
 - `v[2]` # access second element of vector `v` (1-based counting!)
* Different datatypes can be combined using a __list__

## R Data Frames

* The key datatype in R for handling data is the "data frame"
* A data frame consists of named columns
* Different columns can have different types
* Data within same column must be of same type
* Vectors corresponding to columns must have same length
* Data frames are often loosely refered to as a "tables" (may lead to confusion w.r.t another R datatype called table)

```{r}
mtcars # predefined dataset of car models
```

## Predefined Data Sets

* R has more than 100 data sets predefined
* The are part of *preloaded* R package "datasets"
* Obtain a list of preloaded datasets with R command `data()`
* See <https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/00Index.html>
* Examples (type within R): 
* `ToothGrowth` : Tooth growth in Guinea Pigs treated with vitamin C
* `Theoph` : Pharmacokinetics of asthma drug Theophilin
* `CO2` : CO<sub>2</sub> uptake rate of one plant species under different conditions
* Obtain detailed info with `help` or `?` as in `help(CO2)` or `?CO2`
* More datasets are available with additional R packages.

## The "CO2" Data Set

"CO2" is a data frame with data corresponding to CO<sub>2</sub> uptake of one plant species __Echinochloa crus-galli__ . It is frequently considered a "weed".

```
CO2  # also try head(CO2)
```
```{r, echo=FALSE}
kable(head(CO2,n=3))
```

Try within R:

* `nrow(CO2); ncol(CO2) # Rows and columns of table`
* `CO2[2,5]; CO2[2,]; CO2[,5]; CO2[,"conc"];CO2$conc # access`
* `head(CO2) # only show first lines`  
* `help(CO2) # view background information`

## ggplot2 - Basics

* The ggplot2 R package implements the "grammar of graphics" approach
* Example of "extreme modularity"
* PRO: capable, flexible, extensible
* CON: not always very user-friendly
* load ggplot2 with R command: `library(ggplot2)`
* *IF* ggplot2 is not installed, install with `install.packages("ggplot2")` 

## Scatter Plots

* Adding a layer with `geom_point` results in a scatter plot

```{r}
library(ggplot2)
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point()
```

## Controlling Size and Color of Plotted Elements

* Parameter `size` controls the size of the plotted points 
* Parameter `colour` controls the color via name, hexadecimal or data.
* R colors: <http://sape.inf.usi.ch/quick-reference/ggplot2/colour>
```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point(size=3,colour="red")
```

## Mapping Point Shape and Color to Data

* Parameter `shape` controls shape of the plotted points (square,star,circle etc)
* Parameter `colour` can be mapped to a variable 
* Custom colors can be defined "palettes" (various commands, not covered here)
```{r}
ggplot(CO2, aes(x=conc,y=uptake, colour=Type,shape=Treatment)) +
  geom_point(size=2)
```

## Axis Labels and Title

* We can control axis labels with `xlab` and `ylab`.
* We can add a title with `ggtitle`

```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point() +
   xlab("Concentration") + ylab("Uptake") + ggtitle("Uptake vs Concentration")
```


## Tuning Plots with Themes

* We can the "theme" with predefined themes like `theme_classic()`, `theme_dark()` etc.
* We can change font sizes using themes and `base_size`: `theme_classic(base_size=24)`

```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point() + theme_classic(base_size=24)
```


## Exercise

* We saw how to add labels and how to specify size, color of plot elements
* Exercise: create a scatter plot with the same data set (`CO2`) with green dots of size 4, axis labels and `theme_classic` with parameter `base_size` set to 24.
* Bonus: control semi-transparency with parameter `alpha` (a value between 0 and 1). Example: `geom_point(alpha=0.4)`. Why does this example lead to amounts of saturation for different plotted points?
* Bonus: try the different themes: `theme_grey()`,`theme_dark()`, `theme_bw()`, `theme_minimal()`. Find all defined themes by typing `theme_` and then pressing the `TAB` key.

## Exercise (continued)

* Demo of how `geom_point` can be controlled via parameters
* `colour` parameter: hexadecimal `"#00FF00"` is equivalent to string "green"


```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point(size=4,colour="#00FF00",alpha=0.4) +
  theme_classic(base_size=24) + xlab("Concentration")+ ylab("Uptake")
```


## Other 2D Geometries: Jitter Plots

* The scatter plot has the problem that points with the same x,y values are plotted at same location
* Workaround 2: use "jitter plot" with randomly created coordinate offsets (command `geom_jitter`)

```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_jitter()
```

## Other 2D Geometries: Counts Chart

* The scatter plot has the problem that points with the same x,y values are plotted at same location
* Workaround: use semi-transparency
* Workaround: use "counts chart" with randomly created coordinate offsets (command `geom_count`)
```{r}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_count(alpha=0.4)
```


## Adding a Smoothed Line

* Combine layers with "+"
* `geom_smooth` uses method for local "smoothing" and 95% confidence interval
* here: `geom_point` and `geom_smooth`
* parameter `span` for amount of smoothing; `se` for confidence level

```{r warning=FALSE,message=FALSE}
ggplot(CO2, aes(x=conc,y=uptake)) + geom_point() + geom_smooth()
```

## Arranging Plots with Faceting

* "Facetting" allows to arrange related plots in a grid
* Command `facet_grid( Y ~ X)`
Example
* Use dot (`.`) for not splitting up the data with respect to x or y

```{r}
ggplot(CO2, aes(conc, uptake)) + geom_point() + facet_grid(Treatment ~ Type)
```


## Exercise: Faceting

* Try notations `. ~ Type` or `Treatment ~ .` as arguments for `facet_grid`.
* Create a scatter plot (as before using variables conc, uptake for x and y) where the x-axis is divided into several plots based on different individual plants (column name "Plant")

## ggplot2 - Summary

* "geoms" define type of plot
* palettes define used colors (not covered)
* scales define mapping between data coordinates and x,y pixel coordinates. Example: "+scale_x_log10()" changes x-axis to logarithmic scale.

## "Statistics" Plots

* A variety of charts require data processing
* May involve smoothing, binning and estimating data


## The "ToothGrowth" Dataset

* Dataset `ToothGrowth` describes measured length of teeth of Guinea Pigs 
* Guinea pigs received 3 different doses of vitamin C (3 treatment groups)
*	Variable `len`:	numeric	Tooth length
* Variable `supp`: Type (VC:ascorbic acid or OJ: orange juice).
* Variable `dose`: numeric dose in milligrams/day

```
head(ToothGrowth)
```

```{r echo=FALSE}
kable(head(ToothGrowth))
```

## Box Plots

* Box-whisker plots are effective for exploratory data analysis
* Plot of minimum, 25%th percentile (quartile Q1), median, 75% percentile (quartile Q3), maximum and outliers
* R Workaround: `ToothGrowth$Dose <- as.character(ToothGrowth$dose)`

```{r}
ToothGrowth$Dose <- as.character(ToothGrowth$dose) # treat as categorical
ggplot(ToothGrowth, aes(Dose,len)) + geom_boxplot()
```


## Violin Plots

* Violin plots are effective for exploratory data analysis
* Plot of smoothed density (y) and categorical variable (x)

```{r}
ggplot(ToothGrowth, aes(Dose,len)) + geom_violin()
```


## Exercises: Box-Plots and Violin Plots

* Use what we covered so far to customize the boxplot and violin plot
* Increase label font sizes, and color and size of contour lines

## Semi-transparent Densities

* Choose semi-transparancy via `alpha` parameter

```{r}
ggplot(ToothGrowth, aes(len,group=Dose, fill=Dose)) + 
  geom_density(alpha=0.2)
```

## Densities with "Rug"-Plots

* Rug plots show locations of individual data points as bars on x or y axis
* May be helpful for augmenting plot with additional detail
* Parameter: `sides="tbrl"` (any character of trbl for top,both,right, left)

```{r}
ggplot(ToothGrowth, aes(len,group=Dose, fill=Dose)) + 
  geom_density(alpha=0.2) + geom_rug(aes(colour=Dose))
```

## Histograms

* Violin plots are effective for exploratory data analysis
* Plot of smoothed density (y) and categorical variable (x)
* If "group" parameter is provided, default is *stacked* histogram

```{r}
ggplot(ToothGrowth, aes(len,group=Dose, fill=Dose)) + geom_histogram()
```

## Semi-transparent Histograms

* Choose semi-transparancy via `alpha` parameter
* Specify number of bins (`bins`) to avoid warning
* `position="identity"` leads to overlapping histograms

```{r}
ggplot(ToothGrowth, aes(len,group=Dose, fill=Dose)) +
  geom_histogram(position="identity",alpha=0.4,bins=20)
```

## Semi-transparent Histograms (Dodge)

* Choose "dodging" via `position='dodge'` parameter
* Specify number of bins (`bins`) to avoid warning
* `position="dodge"` leads to "dodged" histogram bars

```{r}
ggplot(ToothGrowth, aes(len,group=Dose, fill=Dose)) +
  geom_histogram(position="dodge",bins=20)
```

## 2D Densities

* A geyser is a hot spring where water comes in contact with volcanic magma, leading to steam and more or less regular eruptions of water
* Old faithful is a geyser in Yellowstone National Park
* Old Faithful erupts every 35 to 120 minutes for 1 1/2 to 5 minutes
* Interesting relationship between duration of geyser eruption and wait time between geyser eruptions.
* R data sets `faithfuld` and `faithful`

```{r}
head(faithful,n=6)
```

## Scatter Plot of Old Faithful Data

```{r}
ggplot(faithful, aes(waiting,eruptions)) + geom_point()
```

## 2D Density of Old Faithful Data

* Commands `geom_tile` and `geom_raster` can plot 2D grids
* Only useful if fill color is specified via `fill`
* R table `faithfuld` contains density estimations based on `faithful`
* Advanced: 2D density estimates can be performed with R function `kde2d` in package `MASS`

```{r}
head(faithfuld,n=3)
```

## A Tile Plot of Old Faithful Densities

```{r}
ggplot(faithfuld, aes(waiting, eruptions,fill=density)) + geom_tile()
```


## Color Palettes

* Good example for learning about color palettes in R and ggplot2
* User-provided vector of color descriptors is interpolated
* Fill color: `scale_fill_gradientn`; line color: `scale_colour_gradientn`

```{r}
ggplot(faithfuld, aes(waiting, eruptions,fill=density)) + geom_tile() +
  scale_fill_gradientn(colours=c("#0000FF","#FFFFFF","#FF0000"))
```

## Predefined R Color Palettes

* R has variety of color palettes predefined (independent of ggplot2)
Example: Command `rainbow(n)` computes `n` color descriptors according to spectrum:
```{r}
rainbow(4)
```

* Other examples:
* `heat.colors(n);terrain.colors(n);topo.colors(n);cm.colors(n)`
* More information via `help(rainbow)`


## Example of Predefined Color Palette 

* `topo.colors` is a predefined color palette corresponding to topographic maps

```{r}
ggplot(faithfuld, aes(waiting, eruptions,fill=density)) + geom_tile() +
  scale_fill_gradientn(colours=topo.colors(100))
```

## Exercise: Color Palettes

* R color palettes: explore results of command `rainbow(3)` or `rainbow(10)`. Explore effect of changing parameter in rainbow function on resulting ggplot output. Explore using some alternative R color palette functions (see list via `help(rainbow)`) such as `heat.colors(n),terrain.colors(n),topo.colors(n),cm.colors(n)`
* Explore effect of "manually" defined color palette such as `c("#FF0000","#00FF00","#0000FF")`

## Density Estimations

* The used dataset `faithfuld` had pre-computed density estimates
* `ggplot2` can be used for density estimates from raw data
* Raw data in R table `faithful`

```{r}
ggplot(faithful, aes(waiting, eruptions)) + stat_density2d()
```
